package com.leith.spaceinvaders;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Bullet {
    private Picture picture;

    public Picture getPicture() {
        return picture;
    }

    public Bullet(Picture picture) {
        this.picture = picture;
        System.out.println("Bullet constructor");
    }

    public void move() throws InterruptedException {
        System.out.println("Bullet move()");
        while(picture.getX() >= 0) {
            picture.translate(-10, 0);
            Thread.sleep(200);
        }
    }
}
