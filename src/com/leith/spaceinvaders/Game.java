package com.leith.spaceinvaders;

import org.academiadecodigo.simplegraphics.graphics.Canvas;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Game {
    public final int MAX_WIDTH;
    public final int MAX_HEIGHT;
    private Player player;
    private Background background;

    public Game() {
        MAX_WIDTH = 1010;
        MAX_HEIGHT = 1410;
    }

    public void init() {
        Canvas.setMaxX(MAX_WIDTH);
        Canvas.setMaxY(MAX_HEIGHT);
        background = new Background(new Picture(10,10,"resources/bg.jpg"));
        player = new Player(new Picture((MAX_WIDTH / 2) - (Player.SPRITE_CENTER / 2),MAX_HEIGHT - 200,"resources/player.png"));

        MyKeyboardHandler myKeyboardHandler = new MyKeyboardHandler(player);
        myKeyboardHandler.init();
    }
}
