package com.leith.spaceinvaders;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Player {
    private Picture picture;
    private Bullet bullet;
    public final static int SPRITE_CENTER = 50;

    public Player(Picture picture) {
        this.picture = picture;
        picture.draw();
    }

    public void moveLeft() {
        if (picture.getX() >= 20) {
            picture.translate(-10, 0);
            picture.draw();
        }
    }

    public void moveRight() {
        if (picture.getX() + 100 <= 1000) {
            picture.translate(10, 0);
            picture.draw();
        }
    }

    public void shoot() throws InterruptedException {
        bullet = new Bullet(new Picture(picture.getX() + SPRITE_CENTER - 5,picture.getY(),"resources/bullet.png"));
        bullet.getPicture().draw();
        bullet.move();
    }
}
