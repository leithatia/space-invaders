package com.leith.spaceinvaders;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Background {
    private Picture picture;

    public Background(Picture picture) {
        this.picture = picture;
        picture.draw();
    }
}
