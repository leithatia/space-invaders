package com.leith.spaceinvaders;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class MyKeyboardHandler implements KeyboardHandler {
    private Player player;

    public MyKeyboardHandler(Player player) {
        this.player = player;
    }

    public void init() {
        Keyboard keyboard = new Keyboard(this);

        int[] keys = new int[]{
            KeyboardEvent.KEY_LEFT,
            KeyboardEvent.KEY_RIGHT,
            KeyboardEvent.KEY_SPACE
        };

        for (int i = 0; i < keys.length; i++) {
            KeyboardEvent keyboardEvent = new KeyboardEvent();
            keyboardEvent.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
            keyboardEvent.setKey(keys[i]);
            keyboard.addEventListener(keyboardEvent);
        }

    }
    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        switch(keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_LEFT:
                player.moveLeft();
                break;
            case KeyboardEvent.KEY_RIGHT:
                player.moveRight();
                break;
            case KeyboardEvent.KEY_SPACE:
                try {
                    player.shoot();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("Space pressed");
                break;
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }
}
